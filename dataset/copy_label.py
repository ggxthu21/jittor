import os
import cv2
import numpy as np

def color_distortion(img_folder, distortion_range=0.5):
    img_files = os.listdir(img_folder)

    for img_file in img_files:
        # load image
        if img_file.startswith('distorted'):
            os.remove(os.path.join(img_folder, img_file))
            continue

        # save the result

def copy_label(img_folder):
    img_files = os.listdir(img_folder)

    for img_file in img_files:
        os.system('cp ' + img_folder + '/' + img_file + ' ' + img_folder + '/distorted_' + img_file)


color_distortion('/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/labels')
copy_label('/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/labels')