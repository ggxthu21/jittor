import os
import cv2
from PIL import Image
import random
import numpy as np

def clean_dataset(img_folder, label_folder, distortion_range=0.5):
    img_files = os.listdir(img_folder)
    label_files = os.listdir(label_folder)

    for img_file in img_files:
        # load image
        if img_file.startswith('distorted'):
            os.remove(os.path.join(img_folder, img_file))
        if img_file.startswith('flipped'):
            os.remove(os.path.join(img_folder, img_file))
        if img_file.startswith('rotated'):
            os.remove(os.path.join(img_folder, img_file))
            continue
    for label_file in label_files:
        # load image
        if label_file.startswith('flipped'):
            os.remove(os.path.join(label_folder, label_file))
        if label_file.startswith('rotated'):
            os.remove(os.path.join(label_folder, label_file))
            continue


def color_distortion(img_folder, distortion_range=0.5):
    img_files = os.listdir(img_folder)

    for img_file in img_files:
        # load image
        img = cv2.imread(os.path.join(img_folder, img_file))

        # convert from BGR (OpenCV default) to YUV color space
        yuv_img = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

        # add a random offset to the U and V channels
        yuv_img[:,:,1] = yuv_img[:,:,1] + (np.random.rand() * distortion_range * 255 - distortion_range * 255 / 2)
        yuv_img[:,:,2] = yuv_img[:,:,2] + (np.random.rand() * distortion_range * 255 - distortion_range * 255 / 2)

        # make sure the values are within the valid range
        yuv_img[:,:,1:3] = np.clip(yuv_img[:,:,1:3], 0, 255)

        # convert back to BGR color space
        distorted_img = cv2.cvtColor(yuv_img, cv2.COLOR_YUV2BGR)

        # save the result
        cv2.imwrite(os.path.join(img_folder, 'distorted_' + img_file), distorted_img)


def random_horizontal_flip(label_folder, img_folder, probability=0.5):
    label_files = os.listdir(label_folder)
    img_files = os.listdir(img_folder)

    for label_file, img_file in zip(label_files, img_files):
        if label_file.startswith('distorted') or img_file.startswith('distorted'):
            continue
        if random.random() < probability:
            label_img = Image.open(os.path.join(label_folder, label_file))
            ref_img = Image.open(os.path.join(img_folder, img_file))

            flipped_label = label_img.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
            flipped_img = ref_img.transpose(Image.Transpose.FLIP_LEFT_RIGHT)

            flipped_label.save(os.path.join(label_folder, 'flipped_' + label_file))
            flipped_img.save(os.path.join(img_folder, 'flipped_' + img_file))


def random_rotate(label_folder, img_folder, angles=[0, 90, 180, 270]):
    label_files = os.listdir(label_folder)
    img_files = os.listdir(img_folder)

    for label_file, img_file in zip(label_files, img_files):
        if label_file.startswith('distorted') or img_file.startswith('distorted') or label_file.startswith('flipped') or img_file.startswith('flipped'):
            continue
        angle = random.choice(angles)

        label_img = Image.open(os.path.join(label_folder, label_file))
        ref_img = Image.open(os.path.join(img_folder, img_file))

        rotated_label = label_img.rotate(angle)
        rotated_img = ref_img.rotate(angle)

        rotated_label.save(os.path.join(label_folder, 'rotated_' + label_file))
        rotated_img.save(os.path.join(img_folder, 'rotated_' + img_file))

print('data enhence start\n')
clean_dataset('/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/imgs', '/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/labels')
color_distortion('/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/imgs')
# random_horizontal_flip('/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/labels', '/root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized/imgs')
print('data enhence ok\n')