import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--input_path", type=str)
parser.add_argument("--niter", type=int,default=5)

args = parser.parse_args()

print("训练生成数据存储在./checkpoints/bs4vae中")
subprocess.call(
    f"python oasis_train.py --name bs4vae --dataset_mode custom --label_dir {args.input_path}/labels --image_dir {args.input_path}/imgs --label_nc 29 --no_instance --use_vae --batchSize 8 --niter {args.niter} --niter_decay 0",
    shell=True,
)
