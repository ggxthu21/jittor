#!/bin/bash

# 设置 CUDA_VISIBLE_DEVICES 环境变量
export CUDA_VISIBLE_DEVICES="0"

# 循环运行命令
for ((i=0; i<=10000; i++))
do
    echo "Running iteration $i"

    # 计算 niter 的值
    niter=$((2*i+1))

    python ../../dataset/color_distortion.py

    # 运行 python train.py 命令，传递 input_path 和计算后的 niter 值
    python train.py --input_path /root/autodl-tmp/.autodl/train/JGAN/dataset/train_resized --niter $niter
done
